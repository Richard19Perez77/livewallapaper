package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class PineTree {

	int treex, treey;
	float treeheight, mainBranchWidth;
	int branches;
	float endHeight, startHeight;
	Paint greenPaint, darkGreyPaint, darkGreenPaint;
	private float width;
	private float height;
	private float centerx;

	public PineTree() {
		greenPaint = new Paint();
		greenPaint.setColor(Color.GREEN);
		greenPaint.setStyle(Style.FILL_AND_STROKE);
		greenPaint.setStrokeWidth(1);

		darkGreyPaint = new Paint();
		darkGreyPaint.setColor(Color.DKGRAY);
		darkGreyPaint.setStyle(Style.FILL_AND_STROKE);
		darkGreyPaint.setStrokeWidth(1);

		darkGreenPaint = new Paint();
		darkGreenPaint.setColor(0xFF228B22);
		darkGreenPaint.setStyle(Style.FILL_AND_STROKE);
		darkGreenPaint.setStrokeWidth(1);
	}

	public void onSurfaceChanged(float w, float h) {
		width = w;
		height = h;
		centerx = width / 2;
		startHeight = height * .15f;
		endHeight = height * .75f;
		mainBranchWidth = width * .25f;
		branches = (int) ((endHeight - startHeight) / 2);
	}

	public void draw(Canvas canvas) {
		for (int i = 0; i < height - endHeight; i++) {
			if (i % 2 == 0) {
				canvas.drawLine(centerx - mainBranchWidth / 2, height - i,
						centerx + mainBranchWidth / 2, height - i,
						darkGreenPaint);
			} else {
				// right side added lines
				canvas.drawLine(centerx - mainBranchWidth / 2, height
						- (i * 1.25f), centerx - mainBranchWidth / 4, height
						- i, darkGreyPaint);

				// left side added lines
				canvas.drawLine(centerx + mainBranchWidth / 2, height
						- (i * 1.25f), centerx + mainBranchWidth / 4, height
						- i, darkGreyPaint);
			}
		}
		
		float dist = 0f;
		for (float i = 0; i < branches; i++) {
			switch ((int) i % 2) {
			case 0:
		
				dist = centerx - (centerx - i);
				// straight line to show shape of tree
				canvas.drawLine(centerx, startHeight + i * 2, centerx + i,
						startHeight + i * 2, greenPaint);

				// diagonal branch downwards
				if (i < branches * .9)
					canvas.drawLine(centerx, startHeight + (i * 2), centerx
							- (i * 1), startHeight + (i * 2.5f), darkGreenPaint);

				// far reaching branch
				canvas.drawLine(centerx, startHeight + i * 2, centerx + i,
						startHeight + i * 2 - i / 3, greenPaint);

				canvas.drawLine(centerx + (dist * .5f), startHeight + i * 2,
						centerx + (dist * .5f) + i / 2, startHeight + i * 2 - i
								/ 3, greenPaint);

				// second branching section
				canvas.drawLine(centerx + (dist * .2f), startHeight + (i * 2),
						centerx + (dist * .2f) + i / 3, startHeight + (i * 2)
								- (i * .5f), greenPaint);

				canvas.drawLine(centerx + (dist * .3f), startHeight + (i * 2),
						centerx + (dist * .2f) + i / 3, startHeight + (i * 2)
								- (i / 2), greenPaint);

				// bottom of tree
				canvas.drawLine(centerx, startHeight + (i * 2), centerx
						- (dist * .5f) + i, startHeight + (i * 2) + (i * .3f),
						greenPaint);
				break;
			case 1:
				dist = centerx - (centerx - i);

				// straight line to show base of tree
				canvas.drawLine(centerx, startHeight + i * 2, centerx - i,
						startHeight + i * 2, greenPaint);

				// diagonal branch downwards
				if (i < branches * .9)
					canvas.drawLine(centerx, startHeight + (i * 2), centerx
							+ (i * 1), startHeight + (i * 2.5f), darkGreenPaint);

				// start of angled branches
				canvas.drawLine(centerx, startHeight + i * 2, centerx - i,
						startHeight + i * 2 - i / 3, greenPaint);

				canvas.drawLine(centerx - (int) (dist * .5), startHeight + i
						* 2, centerx - (int) (dist * .5) - i / 2, startHeight
						+ i * 2 - i / 3, greenPaint);

				// second set of branches
				canvas.drawLine(centerx - (dist * .2f), startHeight + (i * 2),
						centerx - (dist * .2f) - i / 3, startHeight + (i * 2)
								- (i / 2), greenPaint);

				canvas.drawLine(centerx - (dist * .3f), startHeight + (i * 2),
						centerx - (dist * .2f) - i / 3, startHeight + (i * 2)
								- (i / 2), greenPaint);

				// bottom of tree
				canvas.drawLine(centerx, startHeight + (i * 2), centerx
						+ (dist * .5f) - i, startHeight + (i * 2) + (i * .3f),
						greenPaint);
				break;
			}
		}
	}
}