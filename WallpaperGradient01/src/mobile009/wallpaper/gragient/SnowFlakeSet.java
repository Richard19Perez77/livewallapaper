package mobile009.wallpaper.gragient;

import android.graphics.Canvas;

public class SnowFlakeSet {

	SnowFlake[] snowFlakes;

	public void onSurfaceChanged(int width, int height) {
		int flakes = (height > width) ? width / 10 : height / 10;
		snowFlakes = new SnowFlake[flakes];

		for (int i = 0; i < snowFlakes.length; i++) {
			snowFlakes[i] = new SnowFlake();
		}

		for (SnowFlake sf : snowFlakes) {
			sf.onSurfaceChanged(width, height);
		}
	}

	public void draw(Canvas c) {
		for (SnowFlake sf : snowFlakes) {
			sf.draw(c);
		}
	}

	public void updatePhysics() {
		for (SnowFlake sf : snowFlakes) {
			sf.updatePhysics();
		}
	}
}