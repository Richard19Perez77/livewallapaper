package mobile009.wallpaper.gragient;

import android.graphics.Canvas;

public class BgBlueShiftIml implements Background {

	int[] reds = new int[] { 0xff0000ff, 0xff0000cc, 0xff000099, 0xff000066,
			0xff000033, 0xff000000 };
	int currColor = 0;
	int red = 0xff0000;

	boolean upShift = true;

	public void updatePhysics() {
		if (upShift) {
			if (currColor == reds.length - 1) {
				upShift = false;
				currColor--;
			} else {
				currColor++;
			}
		} else {
			if (currColor == 0) {
				upShift = true;
				currColor++;
			} else {
				currColor--;
			}
		}
	}

	public void draw(Canvas c) {
		c.drawColor(reds[currColor]);
	}
}