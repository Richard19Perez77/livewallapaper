package mobile009.wallpaper.gragient;

import android.graphics.Canvas;

class TreeScene {

	PineTree tree = new PineTree();

	public void updatePhysics() {

	}

	public void draw(Canvas canvas) {
		tree.draw(canvas);
	}

	public void onSurfaceChanged(PlotPoints plot, int w, int h) {
		tree.onSurfaceChanged(w, h);
	}
}