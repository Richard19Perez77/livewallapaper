package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class SnowFlake {
	float x, y, r, w, h;
	Paint white = new Paint();
	boolean ready;

	public SnowFlake() {
		white.setColor(Color.WHITE);
		white.setStyle(Style.FILL_AND_STROKE);
		white.setStrokeWidth(1);
	}

	public void onSurfaceChanged(float w, float h) {
		this.w = w;
		this.h = h;
		x = (int) (Math.random() * w);
		y = (int) (Math.random() * h);
		if (h > w) {
			r = w / 100;
		} else {
			r = h / 100;
		}
		ready = true;
	}

	public void draw(Canvas canvas) {
		if (ready)
			canvas.drawRect(x, y, x + r, y + r, white);
	}

	public void updatePhysics() {
		y += 40;
		x += 10;

		// move the flakes slightly left by making the x factor move a bit more
		if (y > h) {
			x = (int) (Math.random() * w * 2) - w;
			y = 0;
		} else if (x > w) {
			x = (int) (Math.random() * w * 1.5) - w;
			y = 0;
		}
	}
}